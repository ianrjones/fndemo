﻿namespace FnDemo.App
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    
	public static class Program
	{
		static void Main(string[] args)
		{
            string inputStr = "2 3 4 5 6";

            int result = ConvertSpacedStringToSum(inputStr);
            int builtInResult = DoItWithBuiltInFeatures(inputStr);

            System.Console.WriteLine($"Sum was: {result}");
            System.Console.WriteLine($"The built-in way result was: {builtInResult}");
		}

        public static int DoItWithBuiltInFeatures(string input)
        {
            return input.Split(' ')
                .Select(s => int.Parse(s))
                .Sum();
        }

        public static int DoItSafely(string input)
        {
            return input.Split(' ')
                .Where(IsNumber)
                .Select(s => int.Parse(s))
                .Aggregate((agg, next) => agg += next);
        }
        
        private static bool IsNumber(string s)
        {
            int i;
            return int.TryParse(s, out i);
        }

        public static int ConvertSpacedStringToSum(string input)
        {
            return SumIntArray(
                    ConvertToIntArray(
                        SplitWithDelimiter(input, ' ')));
        }

        public static string[] SplitWithDelimiter(string input, char delimiter)
        {
            return input.Split(delimiter);
        }

        public static int[] ConvertToIntArray(string[] strings)
        {
            return strings.Select(s => int.Parse(s)).ToArray();
        }

        public static int SumIntArray(int[] arrayToSum)
        {
            return arrayToSum.Sum();
        }
	}	
}
