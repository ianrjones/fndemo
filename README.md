# Getting out of the Testing Func

## You'll need:
- .NET Core SDK (get your flavor at https://www.microsoft.com/net/download/windows)
- A text editor (I recommend VSCode or Sublime with the Omnisharp plugin)

## Goals:
- Show how functional programming principles make OOP code more testable

## To run:
- Using your shell, navigate to FnDemo/
- Execute `dotnet build`
- For tests, execute `dotnet test`
- For program execution:
    - navigate to FnDemo.App/
    - Execute `dotnet run`