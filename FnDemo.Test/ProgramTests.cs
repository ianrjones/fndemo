namespace FnDemo.Test
{
    using System;

    using Xunit;

    using static FnDemo.App.Program;

    public class ProgramTests
    {
        [Fact]
        public void Test_ConvertSpacedStringToSum_Valid()
        {
            string testStr = "2 3 4 5 6";

            int result = ConvertSpacedStringToSum(testStr);

            Assert.True(result == 20);
        }

        [Fact]
        public void Test_SplitWithDelimiter_Valid()
        {
            string delimitedString = "a,b,c,d,e";

            string[] splitStrings = SplitWithDelimiter(delimitedString, ',');

            Assert.True(splitStrings.Length == 5);
            Assert.True(splitStrings[1] == "b");
        }

        [Fact]
        public void Test_ConvertToIntArray_Valid()
        {
            string[] stringsToConvert = 
                new string[] { "2", "3", "4", "5", "6" };

            int[] ints = ConvertToIntArray(stringsToConvert);

            Assert.True(ints.Length == 5);
            Assert.True(ints[2] == 4);
        }

        [Fact]
        public void Test_SumIntArray_Valid()
        {
            int[] ints = new int[] { 1, 2, 3, 4, 5 };

            int sum = SumIntArray(ints);

            Assert.True(sum == 15);
        }

        [Fact]
        public void Test_DoItWithBuiltInFeatures_Valid()
        {
            string input = "2 3 4 5 6";

            int result = DoItWithBuiltInFeatures(input);

            Assert.True(result == 20);
        }

        [Fact]
        public void Test_DoItSafely_Valid()
        {
            string input = "2 3 4 a 6";

            int result = DoItSafely(input);

            Assert.True(result == 15);
        }
    }
}